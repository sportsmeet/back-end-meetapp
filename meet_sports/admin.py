from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(User)
admin.site.register(Category)
admin.site.register(Sport)
admin.site.register(Location)
admin.site.register(GeneralEvent)
admin.site.register(Event)
admin.site.register(UserToUserRelation)
admin.site.register(UserEventRelation)

