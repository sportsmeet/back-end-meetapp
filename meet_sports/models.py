from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager  # Default user class of django
from django.core.validators import MaxValueValidator, MinValueValidator


# To create users
class UserManager(BaseUserManager):
    def create_user(self, username, email, password=None):
        if not email:
            raise ValueError("Users must have an email address!")
        if not username:
            raise ValueError("Users must have and username")
        user = self.model(
            username=username,
            email=self.normalize_email(email),
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        user = self.create_user(
            username=username,
            email=self.normalize_email(email),
            password=password,
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True

        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    username = models.CharField(unique=True, max_length=100)
    email = models.EmailField(max_length=255, unique=True)
    date_joined = models.DateTimeField(auto_now_add=True, null=True)
    last_login = models.DateTimeField(auto_now=True)
    city = models.TextField(max_length=100, blank=True, null=True)
    dni = models.CharField(max_length=9, null=True)
    image = models.ImageField(blank=True, null=True, upload_to="profile_images/", height_field=None, width_field=None,
                              max_length=100, default='/profile_images/default_profile_image.jpeg')
    n_followers = models.IntegerField(null=True, default=0)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    user_user_relation = models.ManyToManyField("self", through='UserToUserRelation', symmetrical=False)
    objects = UserManager()  # To link
    # To create a user using the console
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    # To print user
    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True


class UserToUserRelation(models.Model):
    source = models.ForeignKey(User, on_delete=models.CASCADE, related_name='source')
    target = models.ForeignKey(User, on_delete=models.CASCADE, related_name='target')
    is_report = models.BooleanField(default=False)
    is_follow = models.BooleanField(default=False)
    creation_datetime = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = (('source', 'target', 'is_report', 'is_follow'),)

    def __str__(self):
        if self.is_report:
            return "%s reported %s" % (self.source, self.target)
        else:
            return "%s follows %s" % (self.source, self.target)


class Category(models.Model):
    name = models.CharField(max_length=120, null=False, blank=False)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class Sport(models.Model):
    name = models.CharField(max_length=120, null=False, blank=False)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    image = models.ImageField(blank=True, null=True, upload_to="sport_images/", height_field=None,
                              width_field=None, max_length=100, default='/sport_images/default_sport_image.jpeg')

    def __str__(self):
        return self.name


class Location(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    direction = models.TextField(blank=True, null=True)

    def __str__(self):
        return "Latitude: %s, Longitude: %s Direction: %s" % (self.latitude, self.longitude, self.direction)


class GeneralEvent(models.Model):
    code_event = models.CharField(unique=True, null=True, blank=True, max_length=255, default="-")
    title = models.CharField(max_length=255, null=True, blank=True)
    """ to_field=username es para que devuelva username en vez de id"""
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    """created_by = models.ForeignKey(User, to_field='username', on_delete=models.CASCADE)"""

    description = models.TextField(blank=True, null=True)
    sport = models.ForeignKey(Sport, on_delete=models.CASCADE)
    event_place = models.CharField(null=True, blank=True, max_length=100)
    creation_datetime = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=False)

    city = models.CharField(blank=True, null=True, max_length=100)

    max_participants = models.PositiveSmallIntegerField(blank=True, null=True)

    average_rating = models.FloatField(blank=True, null=True)
    competitive = models.BooleanField(default=False)
    start_date = models.DateField(blank=True, null=True)
    start_time = models.TimeField(blank=True, null=True)

    """Periodicity fields"""
    is_periodic = models.BooleanField(default=False)
    PERIODICITY_CHOICES = [
        ('DAY', 'DAILY'),
        ('WEK', 'WEEK'),
    ]
    periodicity = models.CharField(max_length=3, choices=PERIODICITY_CHOICES, null=True)
    repeat_after = models.IntegerField(null=True)

    @property
    def active_event(self):
        return Event.objects.filter(event_parent=self.id, is_active=True)

    @property
    def inactive_event(self):
        return Event.objects.filter(event_parent=self.id, is_active=False)

    def __str__(self):
        return self.title


    def deactivate_child_event(self):
        my_events = Event.objects.filter(event_parent=self, is_active=True)
        for event in my_events:
            event.is_active = False
            event.save()


class Event(models.Model):
    """event_place = models.ForeignKey(Location, on_delete=models.CASCADE, null=True, blank=True)"""
    event_place = models.CharField(null=True, blank=True, max_length=100)
    event_parent = models.ForeignKey(GeneralEvent, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=False)
    final_score = models.CharField(max_length=255, null=True, blank=True)
    creation_datetime = models.DateTimeField(auto_now=True)
    user_event_relation = models.ManyToManyField(User, through='UserEventRelation')
    start_date = models.DateField(blank=True, null=True)
    start_time = models.TimeField(blank=True, null=True)

    def __str__(self):
        return str(self.event_parent.title)


class UserEventRelation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    rating = models.FloatField(validators=[MinValueValidator(0.0), MaxValueValidator(10)], null=True,
                               blank=True)
    subGroup = models.IntegerField(null=True, blank=True)
    creation_datetime = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = (('user', 'event'),)

    def __str__(self):
        return "User: %s - Event: %s" % (str(self.user.username), str(self.event.event_parent.title))
