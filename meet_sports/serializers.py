from rest_framework import serializers
from meet_sports.models import *

'''PAra reseteo de pass por email'''
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from rest_framework.exceptions import AuthenticationFailed


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

    def create(self, validated_data):
        user = User(**validated_data)
        # Encrypt password
        print(validated_data)
        user.set_password(validated_data['password'])
        user.is_active = True
        user.save()
        return user

    def update(self, instance, validated_data):
        updated_user = super().update(instance, validated_data)
        print(validated_data)
        if 'password' in validated_data.keys():
            updated_user.set_password(validated_data['password'])
        print(validated_data.keys())
        if 'is_active' not in validated_data.keys():
            updated_user.is_active = True

        updated_user.save()
        return updated_user


class UserToUserRelationSerializer(serializers.ModelSerializer):
    source = UserSerializer
    target = UserSerializer

    class Meta:
        model = UserToUserRelation
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class SportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sport
        fields = '__all__'


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = '__all__'


class GeneralEventSerializer(serializers.ModelSerializer):
    sport = SportSerializer
    created_by = UserSerializer
    active_event = serializers.SerializerMethodField('get_active_events')
    inactive_event = serializers.SerializerMethodField('get_inactive_events')
    created_by_instance = serializers.SerializerMethodField('get_created_by_instance')
    sport_instance = serializers.SerializerMethodField('get_sport_instance')

    def get_created_by_instance(self, general_event):
        query_users = User.objects.filter(id=general_event.created_by.id)
        serializer = UserSerializer(instance=query_users, many=True)
        return serializer.data

    def get_sport_instance(self, general_event):
        query_users = Sport.objects.filter(id=general_event.sport.id)
        serializer = SportSerializer(instance=query_users, many=True)
        return serializer.data

    def get_inactive_events(self, general_event):
        query_events = Event.objects.filter(is_active=False, event_parent=general_event)
        serializer = EventSerializer(instance=query_events, many=True)
        return serializer.data

    def get_active_events(self, general_event):
        query_events = Event.objects.filter(is_active=True, event_parent=general_event)
        serializer = EventSerializer(instance=query_events, many=True)
        return serializer.data

    class Meta:
        model = GeneralEvent
        fields = '__all__'

    def validate(self, data):
        if data['is_periodic']:
            is_valid = True
            if 'periodicity' not in data:
                is_valid = False
            if 'repeat_after' not in data:
                is_valid = False
            if not is_valid:
                raise serializers.ValidationError("fields [periodicity, repeat_after] must be "
                                                  "informed")
        return data


    def update(self, instance, validated_data):
        updated_general_event = super().update(instance, validated_data)
        active_son_event = Event.objects.filter(event_parent=instance, is_active=True).first()
        if active_son_event is not None:
            print("active_son_event.id")
            active_son_event.event_place = instance.event_place
            active_son_event.start_date = instance.start_date
            active_son_event.start_time = instance.start_time
            active_son_event.is_active = instance.is_active
            active_son_event.save()
        print(validated_data)
        print(instance.id)
        #if "start_date" in validated_data.keys():
        return updated_general_event



class EventSerializer(serializers.ModelSerializer):
    event_parent = GeneralEventSerializer
    """event_place = LocationSerializer(many=False, read_only=True)"""

    class Meta:
        model = Event
        fields = '__all__'


class UserEventRelationSerializer(serializers.ModelSerializer):
    user = UserSerializer
    event = EventSerializer

    class Meta:
        model = UserEventRelation
        fields = '__all__'


class ResetPasswordByEmailSerializer(serializers.Serializer):
    email = serializers.EmailField(min_length=2)

    class Meta:
        fields = ['email']


class SetPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(min_length=5, max_length=50, write_only=True)
    token = serializers.CharField(required=True, write_only=True)
    uidb64 = serializers.CharField(required=True, write_only=True)

    class Meta:
        fields = ['password', 'token', 'uidb64']

    # Validamos que la contraseña y el token
    def validate(self, data):
        try:
            password = data.get('password')
            token = data.get('token')
            uidb64 = data.get('uidb64')

            id = force_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(id=id)

            if not PasswordResetTokenGenerator().check_token(user, token):
                raise AuthenticationFailed('El link para reinicio de contraseña es invalido', 401)

            # Encriptar contraseña
            user.set_password(password)
            # Guardamos cambios en bd
            user.save()

            return user

        except Exception as e:
            raise AuthenticationFailed('El link para reinicio de contraseña es invalido', 401)
