from background_task import background
from datetime import datetime, timedelta
from background_task.models import Task
from meet_sports.models import GeneralEvent, Event, UserEventRelation


#Task.objects.all().delete()#  TODO: Una vez hecho makemigrations y migrate descomentar y eliminar el TODO.


@background()
def create_new_event(general_event_id):
    print(datetime.now())
    general_event_queryset = GeneralEvent.objects.filter(id=general_event_id)
    general_event = general_event_queryset[0]
    all_events = Event.objects.filter(event_parent=general_event.id)
    for event in all_events:
        event.is_active = False
        event.save()
    if not general_event.is_active:
        return general_event

    new_event = Event(is_active=True, event_parent=general_event, event_place=general_event.event_place)
    new_event.start_date = general_event.start_date
    new_event.start_time = general_event.start_time
    new_event.save()

    user_in_event = UserEventRelation(event=new_event, user=general_event.created_by)
    user_in_event.save()

    if general_event.periodicity == 'DAY':
        general_event.start_date = general_event.start_date + timedelta(days=general_event.repeat_after)
    else:
        general_event.start_date = general_event.start_date + timedelta(weeks=general_event.repeat_after)
    general_event.save()


@background()
def set_inactive_old_events():
    print(datetime.now(), "set_inactive_old_events()")
    all_general_events = Event.objects.all()

    for general_event in all_general_events:
        if general_event.is_active:
            general_event_end_datetime = datetime(year=general_event.start_date.year, month=general_event.start_date.month,
                                                  day=general_event.start_date.day, hour=general_event.start_time.hour,
                                                  minute=general_event.start_time.minute,
                                                  second=general_event.start_time.second)
            if general_event_end_datetime < datetime.now():
                general_event.is_active = False
                general_event.save()
                #general_event.deactivate_child_event()
            """print(general_event_end_datetime)"""




#set_inactive_old_events(repeat=30)
