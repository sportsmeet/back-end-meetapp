from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from meet_sports.serializers import GeneralEventSerializer, UserEventRelationSerializer, EventSerializer
from meet_sports.models import GeneralEvent, Event, UserEventRelation, Sport
from rest_framework import status
from meet_sports.utils.user_view import return_dict
from rest_framework import generics
from rest_framework import filters
from meet_sports.tasks import create_new_event
from background_task.models import Task
from datetime import timedelta
import uuid



@api_view(['POST', 'GET', 'PUT', 'DELETE'])
@permission_classes((IsAuthenticated,))
def general_event_manager(request):
    if request.method == 'POST':
        my_data = request.data.copy()
        my_data['created_by'] = request.user.id
        sports_name = Sport.objects.filter(id=my_data["sport"]).first()
        my_data["code_event"] = str(sports_name) + "-" + str(uuid.uuid4().fields[-1])[:5]
        general_event = GeneralEventSerializer(data=my_data)

        if general_event.is_valid():
            general_event_instance = general_event.save()
            create_event(general_event_instance)
            return Response({'message': 'General event created correctly'}, status=status.HTTP_201_CREATED)

        else:
            return Response(general_event.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'PUT':
        general_event = GeneralEvent.objects.get(id=request.data['id'])
        general_event_serializer = GeneralEventSerializer(general_event, data=request.data, partial=True)
        if general_event_serializer.is_valid():
            general_event_serializer.save()
            return Response({'message': 'General event updated correctly'}, status=status.HTTP_200_OK)
        else:
            return Response(general_event_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'GET':
        general_events = GeneralEvent.objects.all()
        general_event_serializers = GeneralEventSerializer(general_events, many=True)
        return Response(general_event_serializers.data, status=status.HTTP_200_OK)

    elif request.method == 'DELETE':
        general_event = GeneralEvent.objects.get(id=request.data['id'])
        if general_event.created_by.id != request.user.id:
            return Response({'error', "General Event doesn't belongs to you."}, status=status.HTTP_400_BAD_REQUEST)

        if general_event:
            general_event.delete()
            return Response({'message': 'General event deleted correcly'}, status=status.HTTP_200_OK)
        else:
            return Response({'error', 'General Event not found'}, status=status.HTTP_400_BAD_REQUEST)

    content = {
        'user': str(request.user.id),
        'auth': str(request.auth),
    }
    return Response(content)


def create_event(general_event: GeneralEvent):
    all_events = Event.objects.filter(event_parent=general_event.id)
    for event in all_events:
        event.is_active = False
        event.save()

    new_event = Event(is_active=True, event_parent=general_event, event_place=general_event.event_place)
    new_event.start_date = general_event.start_date
    new_event.start_time = general_event.start_time
    if general_event.is_periodic:
        if general_event.periodicity == 'DAY':
            general_event.start_date = general_event.start_date + timedelta(days=general_event.repeat_after)
            create_new_event(general_event.id, repeat=Task.DAILY * general_event.repeat_after,
                             schedule=Task.DAILY * general_event.repeat_after)
        else:
            general_event.start_date = general_event.start_date + timedelta(days=general_event.repeat_after)
            create_new_event(general_event.id, repeat=Task.WEEKLY * general_event.repeat_after,
                             schedule=Task.WEEKLY * general_event.repeat_after)

    new_event.save()
    general_event.save()

    user_in_event = UserEventRelation(event=new_event, user=general_event.created_by)
    user_in_event.save()



@api_view(['GET', 'POST', 'DELETE', 'PUT'])
@permission_classes((IsAuthenticated,))
def join_event_manager(request):
    # Unirse a evento
    if request.method == 'POST':

        my_data = request.data.copy()
        my_data['user'] = request.user.id
        user_event_serializer = UserEventRelationSerializer(data=my_data)

        if user_event_serializer.is_valid():
            user_event_serializer.save()
            event = request.data['event']
            return Response({'message': f'User {request.user.id} joined Event {event} correctly '},
                            status=status.HTTP_201_CREATED)
        else:
            return Response(user_event_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    if request.method == 'PUT':
        user_event = UserEventRelation.objects.get(id=request.data['id'])
        """my_user_event = UserEventRelation.objects.filter(user=request.user.id, event=request.data["event"]).first()
        print(my_user_event)
        if request.data["rating"] is not None:
            my_user_event.rating = request.data["rating"]
            my_user_event.save()
            return Response({'message': 'User-event relation updated correctly'}, status=status.HTTP_200_OK)

        return Response({'message': 'User-event relation could not updated correctly'},
                        status=status.HTTP_400_BAD_REQUEST)"""
        user_event_serializer = UserEventRelationSerializer(user_event, data=request.data, partial=True)
        if user_event_serializer.is_valid():
            user_event_serializer.save()
            return Response({'message': 'User-event relation updated correctly'}, status=status.HTTP_200_OK)
        else:
            return Response(user_event_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    if request.method == 'GET':
        event_list = UserEventRelation.objects.filter(user=request.user.id)
        event_list_serializer = UserEventRelationSerializer(event_list, many=True)
        return return_dict(event_list_serializer)

    if request.method == 'DELETE':
        event = request.data['event']
        event_unfollow = UserEventRelation.objects.filter(user=request.user.id, event=event)
        event_unfollow.delete()
        return Response({'message': f'User {request.user.id} left Event {event} correctly '},
                        status=status.HTTP_201_CREATED)


@permission_classes((IsAuthenticated,))
class GeneralEventSearch(generics.ListAPIView):
    queryset = GeneralEvent.objects.all()
    serializer_class = GeneralEventSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['title', 'competitive', 'sport__name']


class EventSearch(generics.ListAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['code_event']


class UserEventAll(generics.ListAPIView):
    queryset = UserEventRelation.objects.all()
    serializer_class = UserEventRelationSerializer
    filter_backends = [filters.SearchFilter]
