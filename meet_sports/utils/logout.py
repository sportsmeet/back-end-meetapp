from rest_framework.response import Response
from rest_framework.decorators import permission_classes
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from datetime import datetime
from django.contrib.sessions.models import Session


@permission_classes((IsAuthenticated,))
class Logout(APIView):
    def get(self, request):
        token = request.user.auth_token
        if token:
            # Get user through token
            user = token.user
            print(token)
            # Taking sessions that expire time is greater than now
            all_sessions = Session.objects.filter(expire_date__gte=datetime.now())
            # Delete sessions of user
            if all_sessions.exists():
                for session in all_sessions:
                    session_data = session.get_decoded()
                    if user.id == int(session_data.get('_auth_user_id')):
                        session.delete()
            token.delete()
            print("token eliminado")
            return Response({'message': 'User logout correctly!'}, status=status.HTTP_200_OK)

        else:
            return Response({'error': 'Invalid user, not found'}, status=status.HTTP_400_BAD_REQUEST)
