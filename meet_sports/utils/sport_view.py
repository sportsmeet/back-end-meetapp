from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from meet_sports.serializers import CategorySerializer, SportSerializer
from meet_sports.models import Category, Sport
from rest_framework import status, generics


@api_view(['POST', 'GET'])
@permission_classes((IsAuthenticated,))
def category_manager(request):
    if request.method == 'POST':
        category = CategorySerializer(data=request.data)
        if category.is_valid():
            category.save()
            return Response({'message': 'Category created correctly'}, status=status.HTTP_201_CREATED)
        else:
            return Response(category.errors, status=status.HTTP_400_BAD_REQUEST)
    if request.method == 'GET':
        categories = Category.objects.all()
        category_serializers = CategorySerializer(categories, many=True)
        return Response(category_serializers.data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def sport_manager(request):
    if request.method == 'POST':
        sport = SportSerializer(data=request.data)
        if sport.is_valid():
            sport.save()
            return Response({'message': 'Sport created correctly'}, status=status.HTTP_201_CREATED)
        else:
            return Response(sport.errors, status=status.HTTP_400_BAD_REQUEST)


# Para devolver todos los sports
class SportSearch(generics.ListAPIView):
    queryset = Sport.objects.all()
    serializer_class = SportSerializer


@api_view(['POST', 'GET'])
@permission_classes((IsAuthenticated,))
def team_information_manager(request):
    if request.method == 'POST':
        team_information = TeamInformationSerializer(data=request.data)
        if team_information.is_valid():
            team_information.save()
            return Response({'message': 'Team information created correctly'}, status=status.HTTP_201_CREATED)
        else:
            return Response(team_information.errors, status=status.HTTP_400_BAD_REQUEST)
    if request.method == 'GET':
        team_information = TeamInformation.objects.all()
        team_information_serializers = TeamInformationSerializer(team_information, many=True)
        return Response(team_information_serializers.data, status=status.HTTP_200_OK)
