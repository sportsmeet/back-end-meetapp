from django.urls import path
from meet_sports.utils.user_view import user_api_view, user_profile_api_view, following_manager,\
    follower_manager, unfollowing_manager, UserSearch, PasswordTokenCheckAPI, ResetPasswordByEmail,\
    SetPasswordByEmail, report_user
from meet_sports.utils.event_view import general_event_manager, join_event_manager, GeneralEventSearch, EventSearch, UserEventAll
from meet_sports.utils.sport_view import category_manager, sport_manager, team_information_manager, SportSearch
from meet_sports.utils.logout import Logout

urlpatterns = [
    # Usuarios
    path('register/', user_api_view, name='user_api'),
    path('user/', user_profile_api_view, name='user_profile_api'),
    path('user_following/', following_manager, name='user_following_api'),
    path('user_unfollowing/', unfollowing_manager, name='user_unfollowing_api'),
    path('user_follower/', follower_manager, name='user_follower_api'),
    path('user/search/', UserSearch.as_view(), name='user_search'),
    path('logout/', Logout.as_view()),
    path('report/', report_user, name="user_report"),

    #Eventos
    path('general_event/', general_event_manager, name='general_event_manager_api'),
    path('join-event/', join_event_manager, name='general_event_manager_api'),
    path('join-event/all/', UserEventAll.as_view(), name='user-event-all'),
    path('general_event/search/', GeneralEventSearch.as_view(), name='general_event_search_api'),
    path('event/search/', EventSearch.as_view(), name='general_event_search_api'),
    path('category/', category_manager, name='category_manager_api'),
    path('sport/', sport_manager, name='sport_manager_api'),
    path('sport/search/', SportSearch.as_view(), name='sport_search'),


    # Reinicio de contra by email
    path('request-reset-email/', ResetPasswordByEmail.as_view(), name='request-reset-email'), #Envia correo con link
    path('password-reset/<uidb64>/<token>/', PasswordTokenCheckAPI.as_view(), name='password-reset-confirm'),
    path('password-reset-complete/', SetPasswordByEmail.as_view(), name='password-reset-complete')

]
