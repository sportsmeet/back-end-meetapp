from rest_framework.permissions import IsAuthenticated
from meet_sports.models import User, UserToUserRelation
from meet_sports.serializers import UserSerializer, UserToUserRelationSerializer, ResetPasswordByEmailSerializer, \
    SetPasswordSerializer
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status
from rest_framework import generics
from rest_framework import filters

# PAra reseteo de pass por email
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import smart_str, force_str, smart_bytes, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.contrib.sites.shortcuts import get_current_site
from django.urls import reverse

from meet_sports.util import send_email
from meetsports_api import settings


def return_dict(follower_list_serializer):
    response = dict()
    response['count'] = len(follower_list_serializer.data)
    response['body'] = follower_list_serializer.data
    return Response(response, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def report_user(request):
    if request.method == 'POST':
        my_data = request.data.copy()
        my_data['source'] = request.user.id
        my_data['is_report'] = True
        user_to_user_serializer = UserToUserRelationSerializer(data=my_data)

        if user_to_user_serializer.is_valid():
            user_to_user_serializer.save()
            target_user = request.data['target']
            return Response({'message': f'User {request.user.id} reported {target_user} correctly '},
                            status=status.HTTP_201_CREATED)


@api_view(['GET', 'POST'])
@permission_classes((IsAuthenticated,))
def following_manager(request):
    if request.method == 'POST':
        if int(request.user.id) == int(request.data['target']):
            return Response('Target and Source cannot be equal', status=status.HTTP_400_BAD_REQUEST)

        my_data = request.data.copy()
        my_data['source'] = request.user.id
        my_data['is_follow'] = True
        user_to_user_serializer = UserToUserRelationSerializer(data=my_data)

        if user_to_user_serializer.is_valid():
            user_to_user_serializer.save()
            target_user = request.data['target']

            '''Increment your following number'''
            followed_user = User.objects.filter(id=target_user).first()
            if followed_user.n_followers is None:
                followed_user.n_followers = 0
            followed_user.n_followers += 1
            followed_user.save()
            return Response({'message': f'User {request.user.id} follows to {target_user} correctly '},
                            status=status.HTTP_201_CREATED)
        else:
            return Response(user_to_user_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    if request.method == 'GET':
        follower_list = UserToUserRelation.objects.filter(is_follow=True, source=request.user.id)
        follower_list_serializer = UserToUserRelationSerializer(follower_list, many=True)
        return return_dict(follower_list_serializer)


@api_view(['DELETE'])
@permission_classes((IsAuthenticated,))
def unfollowing_manager(request):
    if request.method == 'DELETE':
        target_user = request.data['target']
        to_unfollow = UserToUserRelation.objects.filter(is_follow=True, source=request.user.id,
                                                        target=target_user)
        if len(to_unfollow) == 0:
            return Response({'message': f'{target_user} not found '},
                            status=status.HTTP_400_BAD_REQUEST)
        to_unfollow.delete()
        unfollowed_user = User.objects.filter(id=target_user).first()
        unfollowed_user.n_followers -= 1
        unfollowed_user.save()
        return Response({'message': f'User {request.user.id} unfollows to {target_user} correctly '},
                        status=status.HTTP_201_CREATED)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def follower_manager(request):
    if request.method == 'GET':
        follower_list = UserToUserRelation.objects.filter(is_follow=True, target=request.user.id)
        follower_list_serializer = UserToUserRelationSerializer(follower_list, many=True)
        return return_dict(follower_list_serializer)


@api_view(['POST'])
def user_api_view(request):
    # Register into database
    if request.method == 'POST':
        user_serializer = UserSerializer(data=request.data)
        if user_serializer.is_valid():
            user_serializer.save()
            return Response({'message': 'User registered correctly'}, status=status.HTTP_201_CREATED)

        else:
            return Response(user_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE', 'PATCH'])
@permission_classes((IsAuthenticated,))
def user_profile_api_view(request):
    # Query to take user with id
    user = User.objects.filter(id=request.user.id).first()

    if user:
        # See user details
        if request.method == 'GET':
            user_serializer = UserSerializer(user, context={"request": request})
            print(user_serializer.data)

            return Response(user_serializer.data, status=status.HTTP_200_OK)

        elif request.method == 'PUT':
            user_serializer = UserSerializer(user, data=request.data, partial=True)

            if user_serializer.is_valid():
                user_serializer.save()
                return Response({'message': 'User updated correctly'}, status=status.HTTP_200_OK)
            else:
                return Response(user_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


        # Delete user
        elif request.method == 'DELETE':
            user.delete()
            return Response({'message': 'User deleted correctly'}, status=status.HTTP_200_OK)

    else:
        return Response({'message': 'User not found!'}, status=status.HTTP_400_BAD_REQUEST)


"""
 Search user by username
'^' Starts-with search.
'=' Exact matches.
'@' Full-text search. (Currently only supported Django's PostgreSQL backend.)
'$' Regex search.
"""


class UserSearch(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['username', 'email']


# Envia correo con link
class ResetPasswordByEmail(generics.GenericAPIView):
    serializer_class = ResetPasswordByEmailSerializer

    def post(self, request):

        serializer = self.serializer_class(data=request.data)

        email = request.data['email']
        # Validamos que el email exista y enviamos token para resetear la pass
        if User.objects.filter(email=email).exists():
            # Creacion de email y token
            user = User.objects.get(email=email)
            print(user)
            uidb64 = urlsafe_base64_encode(smart_bytes(user.id))
            token = PasswordResetTokenGenerator().make_token(user)
            current_site = get_current_site(request=request).domain
            relative_link = reverse('password-reset-confirm', kwargs={'uidb64': uidb64, 'token': token})
            redirect_url = request.data.get('redirect_url', '')
            absurl = 'http://' + current_site + relative_link
            email_body = 'Hola, \n Utiliza el link para resetear la contraseña \n' + \
                         absurl + "?redirect_url=" + redirect_url
            data = {'email_body': email_body, 'to_email': user.email,
                    'email_subject': 'Reseteo de contraseña'}
            send_email(data)
            return Response({'succes': 'Se ha enviado un link a tu dirección de correo para resetear la contraseña'},
                            status=status.HTTP_200_OK)
        else:
            return Response({'error': 'Ese email no se encuentra registrado!'},
                            status=status.HTTP_400_BAD_REQUEST)


# Cuiando el usuario hace click en el link, el navegador hace una peticion get para validar el token
class PasswordTokenCheckAPI(generics.GenericAPIView):
    serializer_class = SetPasswordSerializer

    def get(self, request, uidb64, token):

        # Verificamos si el token es valido
        try:
            id = smart_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(id=id)

            # para ver que no ha usado el link mas de una vez:
            if PasswordResetTokenGenerator().check_token(user, token):
                return Response({'succes': True, 'message': 'Credenciales validas', 'uidb64': uidb64, 'token': token},
                                status=status.HTTP_200_OK)
            else:
                return Response({'error': 'El token ya se ha utilizado anteriormente, genera uno nuevo'},
                                status=status.HTTP_401_UNAUTHORIZED)

        except DjangoUnicodeDecodeError as identifier:
            return Response({'error': 'El token ya se ha utilizado anteriormente, genera uno nuevo'},
                            status=status.HTTP_401_UNAUTHORIZED)


class SetPasswordByEmail(generics.GenericAPIView):
    serializer_class = SetPasswordSerializer

    def patch(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({'succes': True, 'message': 'Password reseteada correctamente'}, status=status.HTTP_200_OK)
